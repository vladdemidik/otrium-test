<?php

namespace App;

use App\Logger\Logger;
use App\Reports\Generator\GeneratorManager;

class ConsoleKernel
{
    private App $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function handle(array $argv)
    {
        try {
            $this->app->get(GeneratorManager::class)->generateReport($argv[1] ?? null);
        } catch (\App\Reports\Generator\Exception\UndefinedTypeException $e) {
            $this->app->get(Logger::class)->log($e->getMessage());
        } catch (\Exception $e) {
            $this->app->get(Logger::class)->log($e->getMessage().PHP_EOL.$e->getTraceAsString());
        }
    }
}