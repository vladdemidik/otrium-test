<?php

namespace App\Logger;

interface Logger
{
    public function log($message): void;
}