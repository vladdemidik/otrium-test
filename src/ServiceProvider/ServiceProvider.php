<?php

namespace App\ServiceProvider;

use App\DB\DB;
use App\Logger\ConsoleLogger;
use App\Logger\Logger;

class ServiceProvider extends AbstractServiceProvider
{
    public function init(): void
    {
        $config = parse_ini_file('config.ini');

        $this->app->set(\mysqli::class, function () use ($config){
            return DB::getConnection(
                $config['db_host'] ?? '',
                $config['db_username'] ?? null,
                $config['db_password'] ?? null,
                $config['db_database'] ?? null,
                $config['db_port'] ?? null
            );
        });

        $this->app->set(Logger::class, ConsoleLogger::class);
    }
}