<?php

namespace App\ServiceProvider;

use App\App;
use App\DI\Container;

abstract class AbstractServiceProvider
{
    protected App $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    abstract public function init(): void;
}