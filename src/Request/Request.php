<?php

namespace App\Request;

interface Request
{
    public static function create(array $argv): self;
    public function get($name, $default = null): mixed;
}