<?php

namespace App\Request;

class ConsoleRequest implements Request
{

    private array $parameters;

    public function __construct($parameters = [])
    {
        $this->parameters = $parameters;
    }


    public static function create(array $argv): Request
    {
        return new self($argv);
    }

    public function get($name, $default = null): mixed
    {
        if (!isset($this->parameters[$name])) {
            return $default;
        }

        return $this->parameters[$name];
    }
}