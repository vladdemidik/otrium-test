<?php

namespace App\DB;
class DB
{

    private static ?\mysqli $connection = null;
    public static function getConnection($dbHost, $dbUsername, $dbPassword, $dbDatabase, ?int $dbPort): \mysqli
    {
        if (self::$connection === null) {
            static::$connection = mysqli_connect(
                $dbHost,
                $dbUsername,
                $dbPassword,
                $dbDatabase,
                $dbPort
            );
        }

        return self::$connection;
    }
}