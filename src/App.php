<?php

namespace App;

use App\DI\Container;
use App\Reports\Generator\ServiceProvider;
use App\ServiceProvider\AbstractServiceProvider;
use App\ServiceProvider\ServiceProvider as AppServiceProvider;

class App
{
    private Container $container;

    public function __construct()
    {
        $this->container = new Container();
    }

    public function init(): void
    {
        /** @var AbstractServiceProvider $provider */
        foreach ($this->getServiceProviders() as $provider) {
            (new $provider($this))->init();
        }

        $this->container->set(ConsoleKernel::class, function () {
            return new ConsoleKernel($this);
        });
    }

    public function get($abstract, $parameters = [])
    {
        return $this->container->get($abstract, $parameters);
    }

    public function set($abstract, $concrete = null): void
    {
        $this->container->set($abstract, $concrete);
    }

    private function getServiceProviders(): array
    {
        return [
            AppServiceProvider::class,
            ServiceProvider::class,
        ];
    }
}