<?php

namespace App\Reports\Generator\Exception;
class UndefinedTypeException extends \Exception
{
    public function __construct(array $validGenerators)
    {
        $message = 'Undefined report type. Valid types: '
            .implode(',', array_map(function ($g) { return $g->getType(); }, $validGenerators))
            .'.'.PHP_EOL;
        parent::__construct($message);
    }
}