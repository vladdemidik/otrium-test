<?php

namespace App\Reports\Generator;
abstract class Generator
{
    protected const TYPE = '';

    abstract public function generate();

    public function getType(): string
    {
        return static::TYPE;
    }
}