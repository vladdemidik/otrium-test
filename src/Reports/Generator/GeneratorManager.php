<?php

namespace App\Reports\Generator;
use App\Reports\Generator\Exception\UndefinedTypeException;

class GeneratorManager
{
    /** @var Generator[] */
    private array $generators;

    public function __construct()
    {
        $this->generators = [];
    }

    /**
     * @param Generator[] $generators
     * @return void
     */
    public function registerGenerators(array $generators): void
    {
        $this->generators = $generators;
    }

    /**
     * @throws UndefinedTypeException
     */
    public function generateReport(?string $type = null): void
    {
        if (!$type) {
            foreach ($this->generators as $generator) {
                $generator->generate();
            }

            return;
        }

        foreach ($this->generators as $generator) {
            if ($generator->getType() === $type) {
                $generator->generate();
                return;
            }
        }

        throw new UndefinedTypeException($this->generators);
    }
}