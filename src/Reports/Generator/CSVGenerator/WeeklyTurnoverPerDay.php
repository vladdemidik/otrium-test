<?php

namespace App\Reports\Generator\CSVGenerator;

class WeeklyTurnoverPerDay extends CSVGenerator
{
    protected const TYPE = 'csv_weekly_per_day';

    protected function getQuery(): string
    {
        return 'SELECT date(g.date) as day, ROUND(SUM(turnover)/1.21, 2) as turnover_without_vat  FROM brands
LEFT JOIN gmv g on brands.id = g.brand_id
WHERE date >= "%s" AND date <= "%s"
GROUP BY day
ORDER BY day';
    }

    protected function getColumns(): array
    {
        return ['Date', 'Turnover Without VAT'];
    }
}