<?php

namespace App\Reports\Generator\CSVGenerator;
class CSVData
{

    private array $columnNames;
    private array $data;

    public function __construct(array $columnNames, array $data)
    {
        $this->columnNames = $columnNames;
        $this->data = $data;
    }

    public function getColumnNames(): array
    {
        return $this->columnNames;
    }


    public function getData(): array
    {
        return $this->data;
    }

}