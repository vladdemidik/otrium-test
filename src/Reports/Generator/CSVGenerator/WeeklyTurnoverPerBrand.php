<?php

namespace App\Reports\Generator\CSVGenerator;
class WeeklyTurnoverPerBrand extends CSVGenerator
{
    protected const TYPE = 'csv_weekly_per_brand';

    protected function getQuery(): string
    {
        return 'SELECT brands.name, ROUND(SUM(turnover)/1.21, 2) as turnover_without_vat, date(g.date) as day  FROM brands
LEFT JOIN gmv g on brands.id = g.brand_id
WHERE date >= "%s" AND date <= "%s"
GROUP BY brands.name, day
ORDER BY brands.name';
    }

    protected function getColumns(): array
    {
        return ['Brand Name', 'Turnover Without VAT', 'Date'];
    }
}