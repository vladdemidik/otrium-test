<?php

namespace App\Reports\Generator\CSVGenerator;

use App\Reports\Generator\Generator;

abstract class CSVGenerator extends Generator
{
    protected const DATE_FROM = '2018-05-01';
    protected const DATE_TO = '2018-05-07';
    private \mysqli $db;

    public function __construct(\mysqli $db)
    {
        $this->db = $db;
    }

    protected function getFileName(): string
    {
        return static::TYPE . '_' . self::DATE_FROM . '_' . self::DATE_TO . '.' . 'csv';
    }

    abstract protected function getQuery(): string;

    abstract protected function getColumns(): array;

    protected function getDataForGeneration(): CSVData
    {
        $query = sprintf($this->getQuery(), self::DATE_FROM, self::DATE_TO);
        $this->db->prepare($query);
        $data = $this->db->query($query)->fetch_all();

        return new CSVData($this->getColumns(), $data);
    }

    public function generate()
    {
        $data = $this->getDataForGeneration();

        $file = fopen($this->getFileName(), 'w');
        fputcsv($file, $data->getColumnNames());
        foreach ($data->getData() as $line) {
            fputcsv($file, $line);
        }

        fclose($file);
    }
}