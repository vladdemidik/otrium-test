<?php

namespace App\Reports\Generator;

use App\Reports\Generator\CSVGenerator\WeeklyTurnoverPerBrand;
use App\Reports\Generator\CSVGenerator\WeeklyTurnoverPerDay;
use App\ServiceProvider\AbstractServiceProvider;

class ServiceProvider extends AbstractServiceProvider
{
    public function init(): void
    {
        $this->app->set(GeneratorManager::class, function () {
            $manager = new GeneratorManager();

            // add generators here if needed
            $manager->registerGenerators([
                $this->app->get(WeeklyTurnoverPerDay::class),
                $this->app->get(WeeklyTurnoverPerBrand::class),
            ]);

            return $manager;
        });
    }
}