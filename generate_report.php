<?php

require 'vendor/autoload.php';

$app = new \App\App();
$app->init();


/** @var \App\ConsoleKernel $kernel */
$kernel = $app->get(\App\ConsoleKernel::class);
$kernel->handle($argv);