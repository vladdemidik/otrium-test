# Reports generation

Usage: `php generate_report.php <type>`

### Available types
- csv_weekly_per_brand (brand name, turnover)
- csv_weekly_per_day (day, turnover for all brands)

Run without `type` parameter will generate all available types

All reports generators receiving data from db and storing it in csv format


### Run tests
`./vendor/bin/phpunit --verbose tests`