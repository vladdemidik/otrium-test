<?php

namespace Reports\Generator;

use App\Reports\Generator\CSVGenerator\CSVGenerator;
use PHPUnit\Framework\TestCase;

class CSVGeneratorTest extends TestCase
{


    /**
     * @var CSVGenerator
     */
    private $generator;

    private const TEST_DATA = [['test_data1', 'test_data2']];

    protected function setUp(): void
    {
        parent::setUp();

        $result = $this->createMock(\mysqli_result::class);
        $result->method('fetch_all')
            ->willReturn(self::TEST_DATA);

        $mysqli = $this->createMock(\mysqli::class);
        $mysqli->method('query')
                ->willReturn($result);


        $this->generator = new class($mysqli) extends CSVGenerator {

            public const TEST_FILE = 'file.csv';
            public const COLUMNS = ['test1', 'test2'];

            protected function getQuery(): string
            {
                return '';
            }

            protected function getColumns(): array
            {
                return self::COLUMNS;
            }

            protected function getFileName(): string
            {
                return self::TEST_FILE;
            }
        };
    }

    public function testFileCreating()
    {
        $this->generator->generate();
        $this->assertTrue(file_exists($this->generator::TEST_FILE));
        unlink($this->generator::TEST_FILE);
    }

    public function testData()
    {
        $this->generator->generate();
        $f = fopen($this->generator::TEST_FILE, 'r');
        $this->assertEquals($this->generator::COLUMNS, fgetcsv($f));
        $this->assertEquals(self::TEST_DATA[0], fgetcsv($f));
        fclose($f);
        unlink($this->generator::TEST_FILE);
    }

}