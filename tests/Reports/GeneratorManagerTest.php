<?php

namespace Reports;

use App\Reports\Generator\Exception\UndefinedTypeException;
use App\Reports\Generator\Generator;
use App\Reports\Generator\GeneratorManager;
use PHPUnit\Framework\TestCase;

class GeneratorManagerTest extends TestCase
{

    private const VALID_TYPES = [
        'test_type_1',
        'test_type_2',
        'test_type_3',
    ];

    private GeneratorManager $manager;

    protected function setUp(): void
    {
        parent::setUp();
        $this->manager = new GeneratorManager();

        $generators = [];
        foreach (self::VALID_TYPES as $type) {
            $generators []= new class($type) extends Generator {

                private string $type;

                public function __construct($type)
                {
                    $this->type = $type;
                }

                public function generate()
                {
                }

                public function getType(): string
                {
                    return $this->type;
                }
            };
        }

        $this->manager->registerGenerators($generators);
    }

    public function testGenerate()
    {
        $this->manager->generateReport(self::VALID_TYPES[0]);
        $this->manager->generateReport(self::VALID_TYPES[1]);
        $this->manager->generateReport(self::VALID_TYPES[2]);
        $this->manager->generateReport();


        $this->expectException(UndefinedTypeException::class);
        $this->manager->generateReport('invalid_type');
    }

}