<?php

namespace DI;

use App\DI\Container;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{
    private Container $container;
    protected function setUp(): void
    {
        parent::setUp();
        $this->container = new Container();
    }

    public function testGet()
    {
        $className = \stdClass::class;
        $actual = $this->container->get($className);

        $this->assertEquals($className, $actual::class);
    }

    public function testSet()
    {
        $class = new class() {};
        $testName = 'Test';

        $this->container->set($testName, $class);
        $actual = $this->container->get($testName);

        $this->assertEquals($class::class, $actual::class);
    }
}